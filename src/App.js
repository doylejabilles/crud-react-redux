import React, { Component } from "react";
import ActivityList from './components/Activities/ActivityList';
import SideBar from './components/UI/SideBar';


const DUMMMY = [
    { id:'1', color: 'Red', attribute: 'Test', attributeValue: 'N/A',  targetHandlingTime: 'Test'},
    { id:'2', color: 'Red', attribute: 'Test', attributeValue: 'N/A',  targetHandlingTime: 'Test'}
];

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activities: DUMMMY
        }
    }

    render(){        
        return(
            <div className="container-fluid">
                <div className="row">
                    <SideBar/>
                    <ActivityList activities={this.state.activities}/>
                </div>
            </div>
        );
    }
}

export default App;