import React, { Component } from "react";
import { fetchOferring } from '../Store/actions'
import { connect } from 'react-redux';
import $ from 'jquery';

class Offerings extends Component {

    componentDidMount() {
        this.props.fetchOfferings();
    }

    componentDidUpdate() {
        this.processDatafromProps();
    }

    onChangeHandler = (e) => {
        this.props.offeringsHandler(e.target.value)
    }

    processDatafromProps() {
        if( this.props.offeringsData ) {
            let offeringsData = this.props.offeringsData;
            $('#offering option[value="'+offeringsData+'"').prop('selected', true);
        }
    }

    render() {
        return(
            
            <select className="form-select" defaultValue="" name="offering" id="offering" onChange={this.onChangeHandler}>
                <option value="" >-- select --</option>
                {
                    this.props.offerings.map(item => {
                        return(
                            <option key={item.id} value={item.id} >{item.title}</option>
                        );
                    })
                }
            </select>
        );
    }
}

const mapStateToProps = ( state => {
    return {
      offerings: state.activities.offerings
    };
  });
  
  const mapsDispatchtoProps = ( (dispatch) => {
      return {
        fetchOfferings: () => {  dispatch(fetchOferring()) }
      }
    });

export default connect(mapStateToProps, mapsDispatchtoProps) (Offerings);