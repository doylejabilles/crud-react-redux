import React, { Component } from "react";
import { fetchSubProcess } from '../Store/actions'
import { connect } from 'react-redux';
import $ from 'jquery';

class SubProcess extends Component {

    componentDidMount() {
        this.props.fetchSubProcess();
    }

    componentDidUpdate() {
        this.processDatafromProps();
    }

    onChangeHandler = (e) => {
        this.props.subProcessHandler(e.target.value);
    }

    processDatafromProps() {
        if( this.props.subProcessData ) {
            let subProcessData = this.props.subProcessData;
            $('#subProcess option[value="'+subProcessData+'"').prop('selected', true);
        }
    }

    
    render() {
        return(
            
            <select className="form-select" defaultValue="" id="subProcess" name="subProcess" onChange={this.onChangeHandler}>
                <option value="" >-- select --</option>
                {
                    this.props.subProcess.map(item => {
                        return(
                            <option key={item.id} value={item.id} >{item.title}</option>
                        );
                    })
                }
            </select>
        );
    }
}

const mapStateToProps = ( state => {
    return {
        subProcess: state.activities.subProcess
    };
  });
  
  const mapsDispatchtoProps = ( (dispatch) => {
      return {
        fetchSubProcess: () => {  dispatch(fetchSubProcess()) }
      }
    });

export default connect(mapStateToProps, mapsDispatchtoProps) (SubProcess);