import React, { Component } from "react";
import { fetchGroups } from '../Store/actions'
import { connect } from 'react-redux';
import $ from 'jquery';

class Groups extends Component {

    componentDidMount() {
        this.props.fetchGroups();
    }

    componentDidUpdate() {
        this.processDatafromProps();
    }

    onChangeHandler = (e) => {
        this.props.assignGroupHandler(e.target.value);
    }

    processDatafromProps() {
        if( this.props.groupsData ) {
            let groupsData = this.props.groupsData;
            $('#groups option[value="'+groupsData+'"').prop('selected', true);
        }
    }
    
    render() {
        
        
        return(
            <select className="form-select" defaultValue="" id="groups" name="groups" onChange={this.onChangeHandler}>
                <option value="">-- select --</option>
                {
                    this.props.groups.map(item => {
                        return(
                            <option key={item.id} value={item.id}>{item.title}</option>
                        );
                    })
                }
            </select>
        );
    }
}

const mapStateToProps = ( state => {
    return {
        groups: state.activities.groups
    };
  });
  
  const mapsDispatchtoProps = ( (dispatch) => {
      return {
        fetchGroups: () => {  dispatch(fetchGroups()) }
      }
    });

export default connect(mapStateToProps, mapsDispatchtoProps) (Groups);