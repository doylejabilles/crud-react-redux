import React, { Component } from "react";
import { fetchColor } from '../Store/actions'
import { connect } from 'react-redux';
import $ from 'jquery';

class Color extends Component {

    componentDidMount() {
        this.props.fetchColor();

        $('#color').on('change', function(){
            let color = $('#color :selected').val();
            $('#color').css({ 'backgroundColor': color });
        });
    }

    componentDidUpdate() {
        this.processDatafromProps();
    }

    onChangeHandler = (e) => {
        this.props.colorHandler(e.target.value);
    }

    processDatafromProps() {
        if( this.props.colorData ) {
            let colorData = this.props.colorData;
            $('#color option[value="'+colorData+'"').prop('selected', true);
        }
    }

    
    render() {
        
        let color = this.props.colorData;
        $('#color').css({ 'backgroundColor': color });

        return(
            <select className="form-select" defaultValue="" id="color" name="color" onChange={this.onChangeHandler}>
                <option value="" >-- select --</option>
                {
                    this.props.color.map(item => {
                        return <option key={item.id} value={item.id} style={{ backgroundColor : `${item.id}` }} ></option>
                        
                    })
                }
            </select>
        );
    }
}

const mapStateToProps = ( state => {
    return {
        color: state.activities.color
    };
  });
  
  const mapsDispatchtoProps = ( (dispatch) => {
      return {
        fetchColor: () => {  dispatch(fetchColor()) }
      }
    });

export default connect(mapStateToProps, mapsDispatchtoProps) (Color);