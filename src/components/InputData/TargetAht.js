import React, { Component } from 'react';
import { fetchTargetAht } from '../Store/actions'
import { connect } from 'react-redux';
import $ from 'jquery';


class TargetAht extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hour: ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"],
            min: ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60"],
        }
    }

    componentDidMount() {
        this.props.fetchTargetAht();
    }

    // onChangeHandler = (e) => {
    //     this.props.targetAhtHandler(e.target.value);
    // }

    targetAhtHourHandler = (e) => {
        let targetAhtHour = $('#targetAhtHour :selected').val();
        let targetAhtMin = $('#targetAhtMin :selected').val();

        let targetAht = targetAhtHour + ':' + targetAhtMin;

        if( targetAhtHour === '00' && targetAhtMin === '00' ) {
            this.props.targetAhtHandler('');
        } else {
            this.props.targetAhtHandler(targetAht);
        }
    }

    targetAhtMinHandler = (e) => {
        let targetAhtHour = $('#targetAhtHour :selected').val();
        let targetAhtMin = $('#targetAhtMin :selected').val();
        
        let targetAht = targetAhtHour + ':' + targetAhtMin;

        if( targetAhtHour === '00' && targetAhtMin === '00' ) {
            this.props.targetAhtHandler('');
        } else {
            this.props.targetAhtHandler(targetAht);
        }
        
    }

    processDatafromProps() {
        if( this.props.targetAhtData ) {
            let ahtArr = this.props.targetAhtData.split(':');
            $('#targetAhtHour option[value="'+ahtArr[0]+'"').prop('selected', true);
            $('#targetAhtMin option[value="'+ahtArr[1]+'"').prop('selected', true);
        }
    }

    render() {

        this.processDatafromProps();

        return (
            <div className="row">
                <div className="col-md-3">
                    <input type="hidden" id="targetAht" value={this.state.targetAht}/>
                    <select className="form-select" aria-label="Default select example" name="targetAht" id="targetAhtHour" onChange={this.targetAhtHourHandler}>
                        <option value="00" defaultValue>00</option>
                        { 
                            this.state.hour.map((item) => {
                                return <option key={item} value={item} >{item}</option>
                            })
                        }
                    </select> 
                </div>
                <div className="col-md-1">:</div>
                <div className="col-md-3">
                    <select className="form-select" aria-label="Default select example" defaultValue="00" name="targetAht" id="targetAhtMin" onChange={this.targetAhtMinHandler}>
                    <option value="00" >00</option>
                    { 
                        this.state.min.map((item) => {
                            return <option key={item} value={item} >{item}</option>
                        })
                    }
                    </select> 
                </div>
            </div>
        );
    }

}

const mapStateToProps = ( state => {
    return {
        targetAht: state.activities.targetAht
    };
  });
  
  const mapsDispatchtoProps = ( (dispatch) => {
      return {
        fetchTargetAht: () => {  dispatch(fetchTargetAht()) }
      }
    });

export default connect(mapStateToProps, mapsDispatchtoProps) (TargetAht);