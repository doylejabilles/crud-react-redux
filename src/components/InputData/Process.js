import React, { Component } from "react";
import { fetchProcess } from '../Store/actions'
import { connect } from 'react-redux';
import $ from 'jquery';

class Process extends Component {

    componentDidMount() {
        this.props.fetchProcess();
    }

    componentDidUpdate() {
        this.processDatafromProps();
    }

    onChangeHandler = (e) => {
        this.props.processHandler(e.target.value);
    }

    processDatafromProps() {
        if( this.props.processData ) {
            let processData = this.props.processData;
            $('#process option[value="'+processData+'"').prop('selected', true);
        }
    }
    
    render() {
        return(
            
            <select className="form-select"  name="process" id="process" defaultValue="" onChange={this.onChangeHandler}>
                <option value="" >-- select --</option>
                {
                    this.props.process.map(item => {
                        return(
                            <option key={item.id} value={item.id}>{item.title}</option>
                        );
                    })
                }
            </select>
        );
    }
}

const mapStateToProps = ( state => {
    return {
        process: state.activities.process
    };
  });
  
  const mapsDispatchtoProps = ( (dispatch) => {
      return {
        fetchProcess: () => {  dispatch(fetchProcess()) }
      }
    });

export default connect(mapStateToProps, mapsDispatchtoProps) (Process);