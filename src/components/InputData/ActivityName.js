import React, { Component } from "react";
import { fetchActivityName } from '../Store/actions'
import { connect } from 'react-redux';
import $ from 'jquery';

class ActivityName extends Component {

    componentDidMount() {
        this.props.fetchActivityName();
    }

    componentDidUpdate() {
        this.processDatafromProps();
    }

    onChangeHandler = (e) => {
        let displayText = ( e.target.value ) ? $('#activityName :selected').text() : '';
        this.props.activityNameHander(e.target.value, displayText);
    }

    processDatafromProps() {
        if( this.props.activityNameHander ) {
            let activityNameData = this.props.activityNameData;
            $('#activityName option[value="'+activityNameData+'"').prop('selected', true);
        }
    }

    
    render() {
        return(
            
            <select className="form-select" id="activityName" name="activityName" defaultValue="" onChange={this.onChangeHandler}>
                <option value="">-- select --</option>
                {
                    this.props.activityName.map(item => {
                        return(
                            <option key={item.id} value={item.id} >{item.title}</option>
                        );
                    })
                }
            </select>
        );
    }
}

const mapStateToProps = ( state => {
    return {
        activityName: state.activities.activityName
    };
  });
  
  const mapsDispatchtoProps = ( (dispatch) => {
      return {
        fetchActivityName: () => {  dispatch(fetchActivityName()) }
      }
    });

export default connect(mapStateToProps, mapsDispatchtoProps) (ActivityName);