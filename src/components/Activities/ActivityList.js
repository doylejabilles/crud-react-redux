import React, { Component } from "react";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faLongArrowAltRight, faLongArrowAltLeft  } from '@fortawesome/free-solid-svg-icons'
import { fetchActivities, removeActivity, fetchActivityById, emptySelectedActivity } from '../Store/actions';
import Swal from 'sweetalert2';
import FormModal from '../UI/FormModal';
import Lists from './Lists'
import '../../components.css'
// import $ from 'jquery';

class ActivityList extends Component {
    constructor(props) {
        super(props);
        const { activities } = props;

        this.state = {
            activities: activities,
            activity: [],
            offerings: [],
            activityIds: [],
            tableIndexes: [],
            indexesToBeRemoved: [],
            currentPage: 1,
            numberPerPage: 10,
            totalPages: 0,
            activityId: '',
            listIndex: '',
            show: false,
        }
    }

    componentDidMount() {
        this.props.fetchActivities();

        this.setState({
            totalPages: Math.ceil(this.props.activities.length / this.state.numberPerPage)
        })
    }

    componentDidUpdate() {
        
    }

    //SHOW MODAL
    showModalHandler = (value, idx, listIndex) => {
        this.setState({
            show: true,
            activityId: value,
            listIndex: listIndex,
            indexesToBeRemoved: [],
            tableIndexes: []
        });

        if( value ) {
            this.props.fetchActivityById(value);
        }
    }

    //CLOSE MODAL
    closeModalHandler = () => {
        this.setState({
            show: false
        });

        this.setState({
            activityId: ''
        })
        this.props.emptySelectedActivity();
    }

    //GET IDs OF ACTIVITY TO BE DELETED
    getActivityIdsHandler = (checked, value, activityId, index) => {
        if( checked ) {
            this.setState({
                activityIds: [...this.state.activityIds, value],
                indexesToBeRemoved: [...this.state.indexesToBeRemoved, activityId],
                tableIndexes: [...this.state.tableIndexes,  index]
            })
        } else {
            this.setState({
                activityIds: this.state.activityIds.filter( id => id !== value ),
                indexesToBeRemoved: this.state.indexesToBeRemoved.filter( id => id !== activityId ),
                tableIndexes: this.state.tableIndexes.filter( id => id !== index )
            })
        }
    }

    //DELETE ACTIVITY
    deleteActivitiesHandler = () => {

        let ids = this.state.activityIds;
        // let indexes = this.state.indexesToBeRemoved;

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn buttonStyle modalButton',
              cancelButton: 'btn buttonStyleDeleteAlert modalButton'
            },
            buttonsStyling: true
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Delete Activity',
            html: "<p style='text-align: justify; font-weight: 500;'>The Activity you are trying to remove is currently being used in the tool. By proceeding with this request, this activity will be removed where it is currently applicable.  <br/><br/>Do you wish to proceed?</p>",
            icon: '',
            showCancelButton: true,
            confirmButtonText: 'Confirm',
            cancelButtonText: 'Cancel',
            reverseButtons: true,
            showCloseButton: true
          }).then((result) => {
            if (result.isConfirmed) {
              swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )

                for( let id in ids ) {
                    this.props.removeActivity(ids[id], this.state.tableIndexes[id]);
                }

                // for( let index in indexes ) {
                //     // $('table .'+indexes[index]).addClass('removed')
                //     // $('table .'+indexes[index]).addClass('removed').hide();
                // }

                // console.log(this.state.tableIndexes);

                this.setState({
                    indexesToBeRemoved: [],
                    tableIndexes: []    
                });
    

            } else if (
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Cancelled',
                '',
                ''
              )
            }
          });

    }

    prevPage = () => {
        if( this.state.currentPage > 1 ) {
            this.setState({
                currentPage: this.state.currentPage - 1
            });
        }
    }

    nextPage = () => {
        
        let totalPages = Math.ceil(this.props.activities.length / this.state.numberPerPage);
        if( this.state.currentPage < totalPages ) {
            this.setState({
                currentPage: this.state.currentPage + 1
            });
        }
    }

    numberPerPage = (e) => {
        this.setState({
            numberPerPage: e.target.value
        })
    }

    render() {

        // console.log(typeof this.props.activities, this.props.activities);
        // console.log(this.props.activities.length, this.state.numberPerPage);

        return(
            <div className="col-md-10">
                <div className="row">
                    <h3 className="col-md-4">
                        Productive Activities
                    </h3>
                    <div className="col-md-6 offset-md-6">
                    <button className="btn col-md-5 float-start buttonStyle" 
                    onClick={ () => this.showModalHandler('') }>
                    Add an Activity
                    </button>

                    <button 
                    className="btn col-md-5 float-end buttonStyleDelete"
                    data-testid="deleteActivity" 
                    onClick={this.deleteActivitiesHandler}
                    disabled={ ( this.state.activityIds.length > 0 ) ? false : true }>
                    {<FontAwesomeIcon icon={faTrash}/>}  Delete
                    </button>
                    
                </div>

                </div>

                <div className="row">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Color</th>
                                <th scope="col">Activity Name</th>
                                <th scope="col">Attribute</th>
                                <th scope="col">Atribute Value</th>
                                <th scope="col">Target Handling Time</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>

                        {
                            this.props.activities.length > 0 ? this.props.activities.map((item, idx) => {
                                return(
                                    <Lists 
                                    tableIndex={`${idx}I${item.id}`}
                                    listIndex={idx}
                                    key={item.id} 
                                    activity={item} 
                                    offerings={this.props.offerings}
                                    indexesToBeRemoved={this.state.indexesToBeRemoved}
                                    showModal={this.showModalHandler}
                                    getActivityIds={this.getActivityIdsHandler}
                                    currentPage={this.state.currentPage}
                                    numberPerPage={this.state.numberPerPage}
                                    totalActivities={this.props.activities.length}
                                    />
                                );
                            }) : <tr><td colSpan="8" className="text-center">No Activities entered</td></tr>
                        } 
                        </tbody>
                    </table>
                    <div className="row" id="pagination">
                    
                            <table>
                                <thead>

                                </thead>
                                <tbody>
                                
                                <tr>
                                    <td width="2%"><button onClick={this.prevPage}>{<FontAwesomeIcon icon={faLongArrowAltLeft}/>}</button></td>
                                    <td width="2%" className="text-center">{ this.state.currentPage }</td>
                                    <td width="2%"><button onClick={this.nextPage}>{<FontAwesomeIcon icon={faLongArrowAltRight}/>}</button></td>
                                    <td width="5%">
                                        Page Size
                                    </td>
                                    <td width="5%">
                                        <select className="" defaultValue={this.state.numberPerPage} onChange={this.numberPerPage}>
                                            <option value="5" defaultValue>5</option>
                                            <option value="10" defaultValue>10</option>
                                            <option value="20" defaultValue>20</option>
                                        </select>
                                    </td>
                                    <td className="text-right">Total Activites: { this.props.activities.length }</td>
                                </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
                

                <FormModal 
                show={this.state.show} 
                closeModal={this.closeModalHandler}
                activity={this.props.activity}
                id={this.state.activityId}
                listIndex={this.state.listIndex}
                />
            </div>
        );
    }

}


const mapStateToProps = ( state => {
  return {
      activities: state.activities.activities,
      activity: state.activities.activity
  };
});

const mapsDispatchtoProps = ( (dispatch) => {
    return {
        fetchActivities: () => { dispatch(fetchActivities()) },
        removeActivity: (activityId, activityIndex) => { dispatch(removeActivity(activityId, activityIndex)) },
        fetchActivityById: (activityId) => { dispatch(fetchActivityById(activityId)) },
        emptySelectedActivity: () => { dispatch(emptySelectedActivity()) }
    }
});

export default connect(mapStateToProps, mapsDispatchtoProps) (ActivityList);