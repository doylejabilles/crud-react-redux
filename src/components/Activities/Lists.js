import React, { Component } from "react";
import $ from 'jquery';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen, faClock } from '@fortawesome/free-solid-svg-icons'
import '../../components.css'


class Lists extends Component {
    constructor(props) {
        super(props);

        const { activity } = props;

        this.state = {
            activity: activity,
        }

    }

    componentDidMount() {

        if( this.props.indexesToBeRemoved.length < 1 ) {
            $('input[type=checkbox]').prop('checked', false);
        }

        this.paginationController();

    }

    checkboxHandler = (e) => {
        let checked = e.target.checked;
        let value = e.target.value;
        let id = e.target.className;
        let index = this.props.listIndex

        this.props.getActivityIds(checked, value, id, index)
    }

    paginationController = () => {
        let iteration = 0
        let page = 1;
        let numberPerPage = this.props.numberPerPage;
        let currentPage = this.props.currentPage;
        let totalActivities = this.props.totalActivities;
        let totalPages= Math.ceil(totalActivities / numberPerPage)
        // let totalPages= 50; // static for now :)
        
        for( let t=1; t<=totalPages; t++ ) {
            // console.log('totalPages: page'+t.toString());
            $('.dataField').removeClass('page'+t.toString());
            
        }

        $('.dataField').each(function(){
            iteration++
            // if( !$(this).hasClass('removed') ) {
                // $(this).removeClass('page'+page);
                $(this).addClass('page'+page);
                if( iteration === parseInt(numberPerPage) ) {
                    iteration = 0;
                    page++;
                }
            // }
        });
        
        $('.dataField').each(function(){
            if( !$(this).hasClass('removed') ) {
                $('.page'+currentPage).show();
                if( !$(this).hasClass('page'+currentPage) ) {
                    $(this).hide();
                }
            }
        });
    }

    editActivityHander = (e) => {
        this.props.showModal(e.currentTarget.value, this.props.tableIndex, this.props.listIndex);
    }

    render() {

        

        this.paginationController();
        $('.removed').hide();
        
        return (
            <tr className={`dataField  ` + this.props.tableIndex}>
                <td><input type="checkbox" value={this.state.activity.id} className={this.props.tableIndex} onClick={this.checkboxHandler}/></td>
                <td><div className="displayColor" style={ {backgroundColor: this.state.activity.color} }></div></td>
                <td>{ this.state.activity.activityNameDisplay }</td>
                <td>{ this.state.activity.attribute }</td>
                <td>{ this.state.activity.attributeValue }</td>
                <td>{<FontAwesomeIcon icon={faClock}/>} &nbsp;&nbsp; { this.state.activity.targetAht }</td>
                <td>
                    <button className="btn penButton" onClick={this.editActivityHander} value={this.state.activity.id}>{<FontAwesomeIcon icon={faPen}/>}
                    </button>
                </td>
            </tr>
        );
    }
}

export default Lists;