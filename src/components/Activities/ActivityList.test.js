import React from "react";
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ActivityList from './ActivityList';
import Lists from "./Lists";
import { Provider } from 'react-redux'
import store from '../Store/index';
import FormModal from '../UI/FormModal'
import { shallow, mount  } from 'enzyme';

describe('Greeting Component', () => {
    test("Test if Activity List is Rendered", () => {
        render(<Provider store={store}><ActivityList/></Provider>);
    });

    test("If component fetching to firebase", async () => {
        // Arrange
        window.fetch = jest.fn();

        // Act
        window.fetch.mockResolvedValueOnce({
            json: async () => [{ id: 'id', color: 'Color' , activityNameDisplay: 'displayName', attribute: 'att', attributeValue: 'attValue', targetAht: 'targetAht'}],
        });
        render(<Provider store={store}><ActivityList/></Provider>);

        // Assert
        const listItemElements = await screen.findAllByRole('row');
        expect(listItemElements).not.toHaveLength(0);

    });

    test("If modal will open if click add activity", () => {
        render(<Provider store={store}><ActivityList/></Provider>);

         // Arrange
        const onHideModalHandler = jest.fn()

        // Act
        const {getByText} = render(
            <Provider store={store}>
            <FormModal onHide={onHideModalHandler}>
                
            </FormModal>
            </Provider>
        )
        // Assert
        expect(getByText('Add an Activity')).toBeInTheDocument();

    });

    test("Test if delete activity button is initally disabled", () => {
        render(<Provider store={store}><ActivityList/></Provider>);
        
        expect(screen.getByTestId('deleteActivity')).toBeDisabled
    });

    // test("Check if delete button will be enabled if selected ID", () => {

    //     render(<Provider store={store}><ActivityList/></Provider>);
    //     const ids = shallow(<ActivityList />); 
        
    //     console.log(ids);
        
        
    //     // if( ids.state('activityIds').toHaveLength ) {
    //     //     expect(screen.getByRole('button')).toBeDisabled
    //     // } else {
    //     //     expect(screen.getByRole('button')).not.toBeDisabled
    //     // }
    // });
});