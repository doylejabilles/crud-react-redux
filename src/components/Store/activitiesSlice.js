import { createSlice } from "@reduxjs/toolkit";
import Swal from 'sweetalert2';

const activitiesSlice = createSlice({
    name: 'activities',
    initialState: {
        offerings: [],
        process: [],
        subProcess: [],
        activityName: [],
        color: [],
        groups: [],
        targetAht: [],
        activities:[],
        activity: [],
        isSubmit: false
    },
    reducers: {
        populateActivities(state, actions) {
            state.activities = [];
            let keys = Object.keys(actions.payload.activities);
            
            for( let key in keys ) {
                let index = keys[key];
                let activity = {};
                activity[index] = actions.payload.activities[index];
                actions.payload.activities[index].id = index
                state.activities.push(actions.payload.activities[index]);
            }

            state.activities.sort((a,b)=>{
                return a.activityNameDisplay > b.activityNameDisplay ? 1 : -1
            });   
        
        },
        addedActivity(state, action){
            let activity = { };
            activity = action.payload.activity
            activity.id = action.payload.activityId.name
            state.activities.push(activity);

            state.activities.sort((a,b)=>{
                return a.activityNameDisplay > b.activityNameDisplay ? 1 : -1
            });  

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'Activity has been saved!',
            });
        },
        updateActivity(state, action) {
            /* For some reason update don't reflect in the react component */
            // let id = action.payload.activity.id;
            // let stateIndex = action.payload.tableIndex;
            // const existingActivity = state.activities[stateIndex];
            // let keys = Object.keys(action.payload.activity);
            
        },
        removeActivity(state, action) {

            let activityId = action.payload.activityId
            // let activityIndex = action.payload.activityIndex

            for ( let i in state.activities ) {
                if( state.activities[i].id === activityId ) {
                    state.activities.splice(i,1);
                }
            }

            state.activities.sort((a,b)=>{
                return a.activityNameDisplay > b.activityNameDisplay ? 1 : -1
            });  
        },
        checkForm(state, action) {
            state.isSubmit = action.payload.bool;
        },
        populateActivity(state, action) {
            state.activity = action.payload.activity;
        },
        emptyActivity(state) {
            state.activity = [];
        },
        populateOfferings(state, actions) {
            state.offerings = actions.payload.offerings;
        },
        populateProcess(state, actions) {
            state.process = actions.payload.process;
        },
        populateSubProcess(state, actions) {
            state.subProcess = actions.payload.subProcess;
        },
        populateActivityName(state, actions) {
            state.activityName = actions.payload.activityName;
        },
        populateColor(state, actions) {
            state.color = actions.payload.color;
        },
        populateGroups(state, actions) {
            state.groups = actions.payload.groups;
        },
        populateTargetAht(state, actions) {
            state.targetAht = actions.payload.targetAht;
        }
    }
});

export const activitiesActions = activitiesSlice.actions;

export default activitiesSlice;