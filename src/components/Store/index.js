import { configureStore } from "@reduxjs/toolkit";
import activitiesSlice from './activitiesSlice';


const store = configureStore({
    reducer: { activities:activitiesSlice.reducer }
});


export default store;