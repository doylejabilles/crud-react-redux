import { activitiesActions } from './activitiesSlice';

export const addActivity = (activity) => {
    return async (dispatch) => {
        const sendRequest = async () => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/activities.json',
            {
                method: 'POST',
                body: JSON.stringify(activity),
            });

            if ( !response.ok ) {
                throw new Error('There is an error while sending the data.');
            }

            const data = await response.json();
            return data;
        }

        try {
            
            const activityId = await sendRequest();

            dispatch(activitiesActions.addedActivity({
                activity: activity,
                activityId: activityId
            }))
        } catch (e) {

        }
    }
}

export const updateActivity = (activityId, activity, tableIndex) => {
    return async (dispatch) => {
        const sendRequest = async () => {
            const response = await fetch(`https://dummy-activities-default-rtdb.firebaseio.com/activities/${activityId}.json`,
            {
                method: 'PATCH',
                body: JSON.stringify(activity),
            });

            if ( !response.ok ) {
                throw new Error('There is an error while sending the data.');
            }

            const data = await response.json();
            return data;
        }

        try {
             await sendRequest();
            // activity.id = activityId
            dispatch(activitiesActions.updateActivity({
                activity: activity,
                activityId: activityId,
                tableIndex: tableIndex
            }))
        } catch (e) {

        }
    }
}

export const removeActivity = (activityId, activityIndex) => {
    return async(dispatch) => {
        const sendRequest = async() => {
            const response = await fetch(`https://dummy-activities-default-rtdb.firebaseio.com/activities/${activityId}.json`,
            {
                method: 'DELETE',
            });

            if( !response.ok ) {
                throw new Error('There is an error in removing the data')
            }

            const data = await response.json();
            return data;
        }


        try {
            await sendRequest();
            dispatch(activitiesActions.removeActivity({
                activityId: activityId,
                activityIndex: activityIndex
            }));
        } catch(e) {
            //console.log(e)
        }
    }
}

export const fetchActivities = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/activities.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const activities = await fetchData();
            dispatch(activitiesActions.populateActivities({
                activities: activities
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}

export const fetchActivityById = (activityId) => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch(`https://dummy-activities-default-rtdb.firebaseio.com/activities/${activityId}.json`);

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const activity = await fetchData();
            dispatch(activitiesActions.populateActivity({
                activity: activity
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}

export const emptySelectedActivity = () => {
    return (dispatch) => {
        dispatch(activitiesActions.emptyActivity())
    }
}

export const validateForm = (bool) => {
    return (dispatch) => {
        dispatch(activitiesActions.checkForm({
            bool: bool
        }));
    }
}

export const fetchOferring = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/inputData.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const offeringData = await fetchData();
            dispatch(activitiesActions.populateOfferings({
                offerings: offeringData.offerings
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}

export const fetchProcess = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/inputData.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const processData = await fetchData();
            dispatch(activitiesActions.populateProcess({
                process: processData.process
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}

export const fetchSubProcess = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/inputData.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const processData = await fetchData();
            dispatch(activitiesActions.populateSubProcess({
                subProcess: processData.subProcess
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}


export const fetchActivityName = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/inputData.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const processData = await fetchData();
            dispatch(activitiesActions.populateActivityName({
                activityName: processData.activityName
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}

export const fetchColor = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/inputData.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const processData = await fetchData();
            dispatch(activitiesActions.populateColor({
                color: processData.color
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}

export const fetchGroups = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/inputData.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const processData = await fetchData();
            dispatch(activitiesActions.populateGroups({
                groups: processData.groups
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}

export const fetchTargetAht = () => {
    return async (dispatch) => {
        const fetchData = async() => {
            const response = await fetch('https://dummy-activities-default-rtdb.firebaseio.com/inputData.json');

            if( !response.ok ) {
                throw new Error("Failed getting data");
            }

            const data = await response.json();
            return data;
        }

        try {
            const processData = await fetchData();
            dispatch(activitiesActions.populateTargetAht({
                targetAht: processData.targetAht
            }))
        } catch(e) {
            // console.log(e);
        }
    }
}