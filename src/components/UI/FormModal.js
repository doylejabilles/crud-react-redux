import React, { Component } from "react";
import { connect } from 'react-redux';
import { Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { addActivity, updateActivity, validateForm } from '../Store/actions';
import Form from "./Form";


class FormModal extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            id: '',
            attribute: '',
            attributeValue: '',
            type: '',
            activityType: '',
            offerings: '',
            process: '',
            subProcess: '',
            activityName: '',
            color: '',
            groups: '',
            targetAht: '',
            activityNameDisplay: '',
            formCount: false,
        }
    }

    componentDidMount() {
        
    }

    componentDidUpdate() {
        this.checkForms();
    }

    //CLOSE MODAL
    onHideModalHandler = () => {
        this.props.validateForm(false);

        this.clearState();
        this.props.closeModal(false);
    }

    //OBSERVE INPUTS
    formObserver = (activity, count) => {
        if( activity ) {
            this.setState(activity);

            if( this.props.id ) {
                this.setState(activity);
            }
        }
        
    }

    //FORM VALIDATION
    checkForms = () => {

        let formFields = [
            `${this.state.attribute}`,
            `${this.state.attributeValue}`,
            `${this.state.type}`,
            `${this.state.activityType}`,
            `${this.state.offerings}`,
            `${this.state.process}`,
            `${this.state.subProcess}`,
            `${this.state.activityName}`,
            `${this.state.color}`,
            `${this.state.groups}`,
            `${this.state.targetAht}`
        ];
        let formFieldCount = 1;
        
        for( let i = 0; i < formFields.length; i++ ) {
            if( formFields[i] ) {
                formFieldCount++;
            } else {
                formFieldCount--;
            }
        }

        if( formFieldCount >= formFields.length ) {
            this.props.validateForm(true);
        } else {
            this.props.validateForm(false);
        }

    }

    //ADD OR UPDATE ACTIVITY
    submitActivity = () => {
        if( this.props.id ) {
            //Edit
            let activity = {};
            activity.id = this.props.id;
            for ( let state in this.state ) {
                if ( !this.state[state] ) continue;
                activity[state] = this.state[state]
            }
            
            this.props.updateActivity(this.props.id, activity, this.props.listIndex);
            setTimeout(() => {
                window.location.reload(false); //Temporary on reboot
            }, 1000);
        } else {

            //Add
            this.props.addActivity(this.state);
        }

        this.setState({
            formCount: false
        });

        this.clearState();
        this.props.closeModal();

        Swal.fire({
            icon: 'success',
            title: '',
            text: 'Activity has been saved!',
        });
    }

    clearState = () => {
        this.setState({
            id: '',
            attribute: '',
            attributeValue: '',
            type: '',
            activityType: '',
            offerings: '',
            process: '',
            subProcess: '',
            activityName: '',
            color: '',
            groups: '',
            targetAht: '',
            activityNameDisplay: '',
        })
    }
    
    render() {

        return(
            <Modal show={this.props.show} onHide={this.onHideModalHandler} size="lg">
                <Modal.Header closeButton>
                <Modal.Title>Activity</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Form formObserver={this.formObserver}
                    activity={this.props.activity}
                    id={this.props.id}
                    />

                </Modal.Body>
                <Modal.Footer>

                <button 
                className="btn buttonStyleDelete" 
                onClick={this.onHideModalHandler}>
                 Cancel
                </button>

                <button className="btn buttonStyle" 
                onClick={this.submitActivity}
                disabled={ this.props.isSubmit || this.props.id ?  false  : true }
                data-testid="submitForm" >
                 Add an Activity
                </button>

                </Modal.Footer>
            </Modal>
        );
    }
}

const mapStateToProps = ( state => {
    return {
        activities: state.activities.activities,
        isSubmit: state.activities.isSubmit
    };
  });
  
const mapsDispatchtoProps = ( (dispatch) => {
    return {
        addActivity: (activity) => {  dispatch(addActivity(activity)) },
        updateActivity: (activityId, activity, tableIndex) => { dispatch(updateActivity(activityId, activity, tableIndex)) },
        validateForm: ( bool ) => { dispatch(validateForm(bool)) }
    }
});

export default connect(mapStateToProps, mapsDispatchtoProps) (FormModal);