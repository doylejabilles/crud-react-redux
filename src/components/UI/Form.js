import React, { Component } from "react";
import { Container, Row, Col } from 'react-bootstrap';
import $ from 'jquery';
import Offerings from '../InputData/Oferrings'
import Process from "../InputData/Process";
import SubProcess from '../InputData/SubProcess';
import ActivityName from '../InputData/ActivityName';
import Color from '../InputData/Color';
import Groups from '../InputData/Groups'
import TargetAht from '../InputData/TargetAht';


class Form extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            id: '',
            activityTypeData: [
                { id: '1', title: 'Productive' },
                { id: '2', title: 'Non-Productive' },
                { id: '3', title: 'Other Productive' },
                { id: '4', title: 'Break-Offering' }
            ],
            typeData: [
                { id:"1", title: 'Activity' },
                { id:"2", title: 'Sub Activity' },
            ]
        };
    }

    attributeHandler = (e) => {
        const regexNoSpecialChar = /^[A-Za-z0-9 ]+$/;
        const validate = regexNoSpecialChar.test(e.target.value)
        const valueLength = e.target.value.length;

        if( validate && valueLength >= 3 ) {
            this.props.formObserver({attribute: e.target.value}, 1);
        } else {
            this.props.formObserver({attribute: ''}, 1);
        }
    }

    attributeValueHandler = (e) => {
        const regexNoSpecialChar = /^[A-Za-z0-9 ]+$/;
        const validate = regexNoSpecialChar.test(e.target.value);
        const valueLength = e.target.value.length;
        
        if( validate && valueLength >= 3 ) {
            this.props.formObserver({attributeValue: e.target.value}, 1);
        } else {
            this.props.formObserver({attributeValue: ''}, 1);
        }
    }

    typeHandler = (e) => {
        this.props.formObserver({type: e.target.value}, 1);
    }

    activityTypeHandler = (e) => {
        this.props.formObserver({activityType: e.target.value}, 1);
    }

    offeringsHandler = (value) => {
        this.props.formObserver({offerings: value}, 1);
    }

    processHandler = (value) => {
        this.props.formObserver({process: value}, 1);
    }

    subProcessHandler = (value) => {
        this.props.formObserver({subProcess: value}, 1);

    }

    activityNameHander = (value, displayText) => {
        this.props.formObserver({activityName: value}, 1);
        this.props.formObserver({activityNameDisplay: displayText}, 1);
    }

    colorHandler = (value) => {        
        this.props.formObserver({color: value}, 1);
    }

    assignGroupHandler = (value) => {
        this.props.formObserver({groups: value}, 1);
    }

    targetAhtHandler = (value) => {
        this.props.formObserver({targetAht: value}, 1);
    }

    processDatafromProps() {
        
        if( this.props.activity.activityType || this.props.activity.type ) {
            let activityType = this.props.activity.activityType;
            let type = this.props.activity.type

            $('#activityType option[value="'+activityType+'"').prop('selected', true);
            $('#type option[value="'+type+'"').prop('selected', true);
        }
    }

    render() {

        this.processDatafromProps();

        return(
            <Container>
                <Row>
                    <Col>
                    <div className="customFormInput">
                        <label>Type <span>*</span></label>
                        <select className="form-select" defaultValue="" id="type" name="type"  onChange={this.typeHandler}>
                            <option value="" defaultValue>-- select --</option>
                            {
                                this.state.typeData.map( (item) => {
                                    return (<option key={item.id} value={item.id}>{item.title}</option>);
                                })
                            }
                        </select>
                    </div>
                    </Col>
                    <Col>
                    <div className="customFormInput">
                        <label>Activity Type <span>*</span></label>
                        <select className="form-select" defaultValue="" id="activityType" name="activityType"  onChange={this.activityTypeHandler}>
                            <option value="" >-- select --</option>
                            {
                                this.state.activityTypeData.map( (item) => {
                                    return (<option key={item.id} value={item.id}>{item.title}</option>);
                                })
                            }
                        </select>
                    </div>
                    </Col>
                </Row>
                <div className="spacer"></div>
                <Row>
                    <Col>
                    <div className="customFormInput">
                        <label>Offering <span>*</span></label>
                        <Offerings offeringsHandler={ this.offeringsHandler }
                        offeringsData={this.props.activity.offerings}/>
                    </div>
                    </Col>
                    <Col>
                    <div className="customFormInput">
                        <label>Process <span>*</span></label>
                        <Process processHandler={ this.processHandler }
                        processData={this.props.activity.process}/>
                    </div>
                    </Col>
                </Row>
                <div className="spacer"></div>
                <Row>
                    <Col>
                    <div className="customFormInput">
                        <label>Sub Process <span>*</span></label>
                        <SubProcess subProcessHandler={this.subProcessHandler}
                        subProcessData={this.props.activity.subProcess}/>
                    </div>
                    </Col>
                    <Col>
                    <div className="customFormInput">
                        <label>Activity Name <span>*</span></label>
                        <ActivityName activityNameHander={this.activityNameHander}
                        activityNameData={this.props.activity.activityName}/>
                    </div>
                    </Col>
                </Row>
                <div className="spacer"></div>
                <Row>
                    <Col>
                    <div className="customFormInput">
                        <label>Attribute <span>*</span></label>
                        <input type="text" className="form-control" id="attribute" name="attribute" onChange={ this.attributeHandler } defaultValue={this.props.activity.attribute} maxLength="200"></input>
                    </div>
                    </Col>
                    <Col>
                    <div className="customFormInput">
                        <label>Attribute Value<span>*</span></label>
                        <input type="text" className="form-control" name="attributeValue" id="attributeValue" onChange={ this.attributeValueHandler } defaultValue={this.props.activity.attributeValue} maxLength="200"></input>
                    </div>
                    </Col>
                </Row>
                <div className="spacer"></div>
                <Row>
                    <Col>
                    <div className="customFormInput">
                        <label>Target AHT <span>*</span></label>
                        <TargetAht targetAhtHandler={this.targetAhtHandler}
                        targetAhtData={this.props.activity.targetAht}/>
                    </div>
                    </Col>
                    <Col>
                    <div className="customFormInput">
                        <label>Color <span>*</span></label>
                        <Color colorHandler={this.colorHandler}
                        colorData={this.props.activity.color}/>
                    </div>
                    </Col>
                </Row>
                <div className="spacer"></div>
                <Row>
                    <Col>
                    <div className="customFormInput">
                        <label>Assign Group <span>*</span></label>
                        <Groups assignGroupHandler={this.assignGroupHandler}
                        groupsData={this.props.activity.groups}/>
                    </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Form;