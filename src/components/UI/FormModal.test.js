import React from "react";
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux'
import store from '../Store/index';
import FormModal from './FormModal';
import { shallow, mount  } from 'enzyme';

describe('Greeting Component', () => { 
    test("Test if Form Modal is being rendered", () => {
        render(<Provider store={store}><FormModal/></Provider>);
    });

    test("Test if modal is rendered", () => {
        render(<Provider store={store}><FormModal/></Provider>);

        const wrapper = shallow(<Provider store={store}><FormModal /></Provider>);
        expect(wrapper.find(FormModal)).toHaveLength(1);

    })
});
    